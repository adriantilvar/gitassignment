import java.util.ArrayList;

public class Utilizator implements Credentials{
    private String username;
    private String email;
    private static int userNumber = 0;

    public Utilizator(String username, String email) {
        this.username = username;
        this.email = email;
    }

    @Override
    public void signUp() {
        userNumber ++;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public static int getUserNumber() {
        return userNumber;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
